package numan.goldenscent;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;
import com.squareup.picasso.Picasso;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class MainActivity extends  AppCompatActivity implements IVLCVout.Callback {
    public final static String TAG = "MainActivity";
    private SurfaceView mSurface;
    private SurfaceHolder holder;
    private LibVLC libvlc;
    private MediaPlayer mMediaPlayer = null;
    private int mVideoWidth;
    private int mVideoHeight;
    int maxVolume = 50;

    TextView PhotoText;
    ImageView image1, image2, image3, pause, play;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);//Remove title bar
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

            setContentView(R.layout.activity_main);

            Initialize();
            muteVolume();


        } catch(Exception ex)
        {
            Log.e("Main Exception Found" , ex + "");
        }
    }

    public void Initialize() {
        try {
            mSurface = (SurfaceView) findViewById(R.id.surface);
            holder = mSurface.getHolder();

            PhotoText = (TextView) findViewById(R.id.PhotoText);
            Typeface myTypeface1 = Typeface.createFromAsset(this.getAssets(), "text.ttf");
            PhotoText.setTypeface(myTypeface1);

            image1 = (ImageView) findViewById(R.id.image1);
            image2 = (ImageView) findViewById(R.id.image2);
            image3 = (ImageView) findViewById(R.id.image3);
            pause = (ImageView) findViewById(R.id.pause);
            play = (ImageView) findViewById(R.id.play);

            Constants.randomArray = new int[7];

            //Generate Random Numbers
            for (int i = 0; i < 7; i++) {
                Constants.randomArray[i] = (int)(Math.random()*200);

                for (int j = 0; j < i; j++) {
                    if (Constants.randomArray[i] == Constants.randomArray[j]) {
                        Constants.randomArray[j] = (int)(Math.random()*200); //What's this! Another random number!
                    }
                }
            }

            LoadImages();
        } catch(Exception ex)
        {
            Log.e("Init Exception Found" , ex + "");
        }
    }

    public void muteVolume() {
        try {
            AudioManager mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            int set_volume = 0;
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, set_volume, 0);
        } catch(Exception ex)
        {
            Log.e("Mute Exception Found" , ex + "");
        }
    }

    public void LoadImages() {
        Picasso.with(this)
                .load(Constants.imageURL+Constants.randomArray[Constants.Index1])
                .error(R.drawable.error)                // optional
                .resize(150,150)                        // optional
                .placeholder(R.drawable.place_holder)
                .into(image1);

        Picasso.with(this)
                .load(Constants.imageURL+Constants.randomArray[Constants.Index2])
                .error(R.drawable.error)                // optional
                .resize(150,150)                        // optional
                .placeholder(R.drawable.place_holder)
                .into(image2);

        Picasso.with(this)
                .load(Constants.imageURL+Constants.randomArray[Constants.Index3])
                .error(R.drawable.error)                // optional
                .resize(150,150)                        // optional
                .placeholder(R.drawable.place_holder)
                .into(image3);
    }

    public void next(View v) {
        try {
            singleAnimator(1);
            Constants.Index1 = Constants.Index2;
            Constants.Index2 = Constants.Index3;
            Constants.Index3 = Constants.Index3 + 1;
            if (Constants.Index3 >= 7) {
                Constants.Index3 = 0;
            }

            Picasso.with(this)
                    .load(Constants.imageURL + Constants.randomArray[Constants.Index1])
                    .error(R.drawable.error)                // optional
                    .resize(150, 150)                        // optional
                    .into(image1);

            Picasso.with(this)
                    .load(Constants.imageURL + Constants.randomArray[Constants.Index2])
                    .error(R.drawable.error)                // optional
                    .resize(150, 150)                        // optional
                    .into(image2);

            Picasso.with(this)
                    .load(Constants.imageURL + Constants.randomArray[Constants.Index3])
                    .error(R.drawable.error)                // optional
                    .resize(150, 150)                        // optional
                    .into(image3);
        } catch(Exception ex)
        {
            Log.e("Next Exception Found" , ex + "");
        }
    }

    public void previous(View v) {
        try {
            singleAnimator(0);
            Constants.Index3 = Constants.Index2;
            Constants.Index2 = Constants.Index1;
            Constants.Index1 = Constants.Index1 - 1;
            if (Constants.Index1 <= 0) {
                Constants.Index1 = 6;
            }

            Picasso.with(this)
                    .load(Constants.imageURL + Constants.randomArray[Constants.Index1])
                    .error(R.drawable.error)                // optional
                    .resize(150, 150)                        // optional
                    .into(image1);

            Picasso.with(this)
                    .load(Constants.imageURL + Constants.randomArray[Constants.Index2])
                    .error(R.drawable.error)                // optional
                    .resize(150, 150)                        // optional
                    .into(image2);

            Picasso.with(this)
                    .load(Constants.imageURL + Constants.randomArray[Constants.Index3])
                    .error(R.drawable.error)                // optional
                    .resize(150, 150)                        // optional
                    .into(image3);
        } catch(Exception ex)
        {
            Log.e("Previous Exception" , ex + "");
        }
    }

    public void pauseVideo(View v) {
        try{
            mMediaPlayer.pause();
            pause.setVisibility(View.GONE);
            play.setVisibility(View.VISIBLE);
            TastyToast.makeText(this, "Video Paused !", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show();
        } catch(Exception ex)
        {
            Log.e("Pause Exception " , ex + "");
        }
    }

    public void playVideo(View v) {
        try{
            mMediaPlayer.play();
            pause.setVisibility(View.VISIBLE);
            play.setVisibility(View.GONE);
            TastyToast.makeText(this, "Video Played !", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show();
        } catch(Exception ex)
        {
            Log.e("Pause Exception " , ex + "");
        }
    }

    public void stopVideo(View v) {
        try{
            pause.setVisibility(View.GONE);
            play.setVisibility(View.VISIBLE);
            mMediaPlayer.stop();
            TastyToast.makeText(this, "Video Stopped !", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show();
        } catch(Exception ex)
        {
            Log.e("Pause Exception " , ex + "");
        }
    }

    public void forwardVideo(View v) {
        try{
            if (Constants.PlayedVideo == 1)
                createPlayer(Constants.mFilePath2);
            else
                createPlayer(Constants.mFilePath1);

            TastyToast.makeText(this, "Please wait, Loading Next Video !", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show();
        } catch(Exception ex)
        {
            Log.e("Pause Exception " , ex + "");
        }
    }

    public void playNext() {
        {
            try {
                if (Constants.PlayedVideo == 1)
                    createPlayer(Constants.mFilePath2);
                else
                    createPlayer(Constants.mFilePath1);
            } catch(Exception ex)
            {
                Log.e("Play Next Exception " , ex + "");
            }
        }
    }

    private void singleAnimator(int status) {


        if(status == 0) {
            ObjectAnimator animatorScaleX = ObjectAnimator.ofFloat(image1, "scaleX", 0.5f, 1f);
            ObjectAnimator animatorScaleY = ObjectAnimator.ofFloat(image1, "scaleY", 0.5f, 1f);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setDuration(400);
            animatorSet.playTogether(animatorScaleX, animatorScaleY);
            animatorSet.setInterpolator(new DecelerateInterpolator(2));
            image1.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            animatorSet.start();
        } else {
            ObjectAnimator animatorScaleX = ObjectAnimator.ofFloat(image3, "scaleX", 0.5f, 1f);
            ObjectAnimator animatorScaleY = ObjectAnimator.ofFloat(image3, "scaleY", 0.5f, 1f);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setDuration(400);
            animatorSet.playTogether(animatorScaleX, animatorScaleY);
            animatorSet.setInterpolator(new DecelerateInterpolator(2));
            image3.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            animatorSet.start();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    protected void onResume() {
        super.onResume();
        createPlayer(Constants.mFilePath1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        releasePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }


    private void setSize(int width, int height) {
        try {
            mVideoWidth = width;
            mVideoHeight = height;
            if (mVideoWidth * mVideoHeight <= 1)
                return;

            if (holder == null || mSurface == null)
                return;

            int w = getWindow().getDecorView().getWidth();
            int h = getWindow().getDecorView().getHeight();
            boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
            if (w > h && isPortrait || w < h && !isPortrait) {
                int i = w;
                w = h;
                h = i;
            }

            float videoAR = (float) mVideoWidth / (float) mVideoHeight;
            float screenAR = (float) w / (float) h;

            if (screenAR < videoAR)
                h = (int) (w / videoAR);
            else
                w = (int) (h * videoAR);

            holder.setFixedSize(mVideoWidth, mVideoHeight);
            ViewGroup.LayoutParams lp = mSurface.getLayoutParams();
            lp.width = w;
            lp.height = h;
            mSurface.setLayoutParams(lp);
            mSurface.invalidate();
        } catch(Exception ex)
        {
            Log.e("Size Exception Found" , ex + "");
        }
    }

    private void createPlayer(String media) {

        try {
            //To Choose Next Video after Completion
            if(media.equals(Constants.mFilePath1))
                Constants.PlayedVideo = 1;
            else
                Constants.PlayedVideo = 2;

            releasePlayer();
            // Create LibVLC
            // TODO: make this more robust, and sync with audio demo
            ArrayList<String> options = new ArrayList<String>();
            //options.add("--subsdec-encoding <encoding>");
            options.add("--aout=opensles");
            options.add("--audio-time-stretch"); // time stretching
            options.add("-vvv"); // verbosity
            libvlc = new LibVLC(this, options);
            holder = mSurface.getHolder();
            holder.setKeepScreenOn(true);

            // Creating media player
            mMediaPlayer = new MediaPlayer(libvlc);
            mMediaPlayer.setEventListener(mPlayerListener);

            // Seting up video output
            final IVLCVout vout = mMediaPlayer.getVLCVout();
            vout.setVideoView(mSurface);
            //vout.setSubtitlesView(mSurfaceSubtitles);
            vout.addCallback(this);
            vout.attachViews();

            Media m = new Media(libvlc, Uri.parse(media));
            mMediaPlayer.setMedia(m);
            mMediaPlayer.play();
            mMediaPlayer.setVolume(0);

        } catch (Exception ex) {
            Log.e("Media Exception", ex + "");
        }
    }

    private void releasePlayer() {
        try {
            if (libvlc == null)
                return;
            mMediaPlayer.stop();
            final IVLCVout vout = mMediaPlayer.getVLCVout();
            vout.removeCallback(this);
            vout.detachViews();
            holder = null;
            libvlc.release();
            libvlc = null;

            mVideoWidth = 0;
            mVideoHeight = 0;
        } catch(Exception ex)
        {
            Log.e("Release Exception" , ex + "");
        }
    }

    /**
     * Registering callbacks
     */
    private MediaPlayer.EventListener mPlayerListener = new MyPlayerListener(this);

    @Override
    public void onNewLayout(IVLCVout vout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0)
            return;

        // store video size
        mVideoWidth = width;
        mVideoHeight = height;
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vout) {

    }

    @Override
    public void onHardwareAccelerationError(IVLCVout vlcVout) {
        Log.e(TAG, "Error with hardware acceleration");
        this.releasePlayer();
        Toast.makeText(this, "Error with hardware acceleration", Toast.LENGTH_LONG).show();
    }

    private class MyPlayerListener extends Application implements MediaPlayer.EventListener {
        private WeakReference<MainActivity> mOwner;

        public MyPlayerListener(MainActivity owner) {
            mOwner = new WeakReference<MainActivity>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            try {
                MainActivity player = mOwner.get();

                switch (event.type) {
                    case MediaPlayer.Event.EndReached:
                        playNext();
//                        player.releasePlayer();
                        break;
                    case MediaPlayer.Event.Playing:
                        break;
                    case MediaPlayer.Event.Paused:
                        break;
                    case MediaPlayer.Event.Stopped:
                        break;
                    default:
                        break;
                }
            } catch(Exception ex)
            {
                Log.e("Event Exception Found" , ex + "");
            }
        }
    }
}
